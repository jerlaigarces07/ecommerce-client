import React, { useContext } from 'react';
import ProductCard from '../components/ProductCard';
import axios from 'axios';
import { Fragment } from 'react';
import { useEffect, useState } from 'react';
import { ProductProvider } from '../ProductContext';
import ProductContext from '../ProductContext';
import { Redirect } from 'react-router-dom';
import Loader from 'react-loader-spinner';

const Admin = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const { setEditProduct } = useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await axios.get(
        'https://salty-bayou-48466.herokuapp.com/products'
      );

      setProducts(response.data);
    } catch (error) {
      console.log('Error occurred', error);
    }
  };

  useEffect(() => {
    setLoading(true);
    const userObject = localStorage.getItem('user');
    setUser(JSON.parse(userObject));
    setLoading(false);
  }, []);

  {
    console.log(',,', !user?.isAdmin, user.isAdmin, user);
  }
  return (
    <>
      {loading ? (
        <div className="flex flex-col w-full h-screen items-center justify-center bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200">
          <Loader type="Hearts" color="#d61818" height={80} width={80} />
          <h1 className="text-gray-800 font-light">Loading.. please wait..</h1>
        </div>
      ) : user === null ? (
        <Redirect to="/login" />
      ) : !user.isAdmin ? (
        <Redirect to="/products" />
      ) : (
        <div className="flex-1 bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-full">
          <div className="flex justify-center">
            <a href="/admin/add">
              <button className="mt-20 p-2 pl-3 pr-3 bg-transparent border-2 border-red-500 text-red-500 text-light rounded-lg hover:bg-red-500 hover:text-gray-100 focus:border-4 focus:border-red-300">
                Add New Product
              </button>
            </a>
          </div>
          <div className="grid grid-row md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-1 md:gap-3 place-items-center mt-10 ml-5 mr-5">
            <Fragment>
              {products.map((data) => (
                <ProductCard
                  setEditProduct={setEditProduct}
                  productName={data.productName}
                  description={data.description}
                  cup={data.cup}
                  photo={data.photo}
                  id={data._id}
                  isAdmin
                  fetchProducts={fetchProducts}
                />
              ))}
            </Fragment>
          </div>
        </div>
      )}
    </>
  );
};

export default Admin;
