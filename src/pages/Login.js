import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import coffee1 from '../images/coffee1.gif';
import Swal from 'sweetalert2';

const Login = (props) => {
  const [user, setUser] = useState(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    fetch('https://salty-bayou-48466.herokuapp.com/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access != 'undefined') {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });

          const userObject = {
            id: data._id,
            isAdmin: data.isAdmin,
          };

          localStorage.setItem('user', JSON.stringify(userObject));
          localStorage.setItem('token', data.access);

          Swal.fire({
            icon: 'success',
            title: 'Login Successful',
            text: 'Enjoy!',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Authentication failed',
            text: 'Check your login details and try again',
          });
        }
      })
      .catch((error) => console.log(error));
  }

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  useEffect(() => {
    const userObject = localStorage.getItem('user');
    setUser(JSON.parse(userObject));
  }, []);

  console.log('user', user?.isAdmin ? '/admin' : '/products');

  return user !== null ? (
    <Redirect to={`${user.isAdmin ? '/admin' : '/products'}`} />
  ) : (
    <>
      <div className="bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-full">
        <div className="flex flex-col items-center mt-20">
          <h1 className="text-xl font-bold text-pink-400 pb-5">
            Welcome back coffee lover!
          </h1>
          <img src={coffee1} alt="coffee.gif" className="w-20 pb-5" />
        </div>

        <div className="container mx-auto flex flex-col items-center">
          <form
            className="shadow-lg w-1/2 p-4 flex flex-col bg-white rounded-lg"
            onSubmit={(e) => authenticate(e)}
          >
            <input
              className="mb-3 py-3 px-4 border border-gray-400 focus:outline-none rounded-md focus:ring-1 ring-cyan-500"
              controlId="userEmail"
              type="text"
              value={email}
              placeholder="Email Address"
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <input
              className="mb-3 py-3 px-4 border border-gray-400 focus:outline-none rounded-md focus:ring-1 ring-cyan-500"
              controlId="password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Pasword"
            />
            <button className="transform hover:scale-110 motion-reduce:transform-none hover:bg-blue-400 w-full bg-blue-500 text-white p-3 rounded-lg font-semibold text-lg">
              Login
            </button>
            <a className="text-blue-400 text-center my-2">Forgot Pasword?</a>
            <hr />
            <a href="/register">
              <button className="transform hover:scale-110 motion-reduce:transform-none hover:bg-green-500 w-full bg-green-400 mt-8 mb-4 text-white p-3 rounded-lg font-semibold text-lg">
                Create New Account
              </button>
            </a>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
