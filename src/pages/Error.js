import React from 'react';
import error from '../images/error.gif';
import { useHistory } from 'react-router';

const Error = () => {
  const history = useHistory();
  return (
    <div className="flex bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-full justify-center place-items-center">
      <div className="flex bg-white justify-center m-20 rounded-2xl w-full">
        <div>
          <h1 className="text-center text-3xl font-semibold m-10">
            404 - Page Not Found
          </h1>
          <img src={error} alt="" />
          <h2 className="font-medium mb-4 text-center">
            The page you're looking for cannot be found.
          </h2>
          <div className="flex w-full mb-10">
            <button
              class="m-auto p-2 pl-5 pr-5 bg-transparent border-2 border-red-500 text-red-500 text-lg rounded-lg hover:bg-red-500 hover:text-gray-100 focus:border-4 focus:border-red-300"
              onClick={() => {
                history.push('/');
              }}
            >
              Go back to home
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Error;
