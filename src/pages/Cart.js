import React, { useState, useEffect } from 'react';
import CartItem from '../components/CartItem';
import { difference } from 'lodash';
import { useHistory, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import Loader from 'react-loader-spinner';

const Cart = () => {
  const [currentUser, setCurrentUser] = useState({});
  const history = useHistory();
  const [cart, setCart] = useState([]);
  const [loading, setLoading] = useState(false);
  const [total, setTotal] = useState(0);
  const [bankVisible, setBankVisible] = useState(false);

  const reduceQuantity = (id, currentCount, cupId) => {
    const filter = cart.filter(
      (item) => item.id === id && item.cup._id === cupId
    );

    if (currentCount === 1) {
      const diff = difference(cart, filter);
      setCart(diff);
      localStorage.setItem('cart', JSON.stringify(diff));
    } else {
      const updatedCart = cart.map((item) => {
        if (item.id === id && item.cup._id === cupId) {
          return {
            ...item,
            count: item.count - 1,
          };
        }
        return item;
      });
      setCart(updatedCart);

      localStorage.setItem('cart', JSON.stringify(updatedCart));
    }
  };

  const checkout = async () => {
    setLoading(true);
    if (cart.length === 0) {
      setLoading(false);
      return Swal.fire({
        icon: 'error',
        title: 'Cannot Process order',
        text: 'You have an empty cart. Please select at least one coffee 😍☕☕☕',
      });
    }

    const data = {
      products: cart,
      userId: currentUser.id,
      totalAmount: total,
    };

    try {
      const response = await axios.post(
        'https://salty-bayou-48466.herokuapp.com/users/checkout',
        data,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        }
      );
      if (response.data.message === 'Order Created') {
        Swal.fire({
          icon: 'success',
          title: 'Order has been successfully processed',
          text: 'Enjoy your coffee! ☕☕☕',
        });
        localStorage.setItem('cart', []);
        history.push('/products');
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Cannot Process order',
          text: 'Something went wrong. Please try again',
        });
      }

      setLoading(false);
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Cannot Process order',
        text: 'Something went wrong. Please try again',
      });
      setLoading(false);
    }
  };

  const addQuantity = (id, cupId) => {
    const updatedCart = cart.map((item) => {
      if (item.id === id && item.cup._id === cupId) {
        return {
          ...item,
          count: item.count + 1,
        };
      }
      return item;
    });
    setCart(updatedCart);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  };

  useEffect(() => {
    const prices = cart.map((item) => item.count * item.cup.price);
    let totalPrice = 0;
    prices.forEach((price) => {
      totalPrice += price;
    });
    setTotal(totalPrice);
  }, [cart]);

  useEffect(() => {
    setCurrentUser(JSON.parse(localStorage.getItem('user')));
    const localCart = localStorage.getItem('cart');
    if (!localCart) return;
    const parsedCart = JSON.parse(localCart);
    if (localCart.length == 0) return;
    setCart(parsedCart);

    const prices = parsedCart.map((item) => item.count * item.cup.price);
    let totalPrice = 0;
    prices.forEach((price) => {
      totalPrice += price;
    });
    setTotal(totalPrice);
  }, []);

  return currentUser === null ? (
    <Redirect to="/login" />
  ) : (
    <div className="bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-banner">
      <section className="grid grid-cols-2 max-w-full p-6 mx-auto bg-cyan-700 rounded-md shadow-md dark:bg-gray-800 mt-20 w-4/5">
        <div className="leading-loose">
          <div className="w-11/12 p-10 bg-white rounded shadow-xl">
            <p className="text-gray-800 font-light text-sm mb-3">
              Complete your shipping and payment details below.
            </p>
            <div className="">
              <label className="block text-sm text-gray-00" htmlFor="cus_name">
                Name
              </label>
              <input
                className="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded"
                id="cus_name"
                name="cus_name"
                type="text"
                required=""
                placeholder="Your Name"
                aria-label="Name"
                required
              />
            </div>
            <div className="mt-2">
              <label
                className="block text-sm text-gray-600"
                htmlFor="cus_email"
              >
                Email
              </label>
              <input
                className="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded"
                id="cus_email"
                name="cus_email"
                type="email"
                required=""
                placeholder="Your Email"
                aria-label="Email"
                required
              />
            </div>
            <div className="mt-2">
              <label
                className=" block text-sm text-gray-600"
                htmlFor="cus_email"
              >
                Address
              </label>
              <input
                className="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded"
                id="cus_email"
                name="cus_email"
                type="text"
                placeholder="Street"
                aria-label="Email"
                required
              />
            </div>
            <div className="mt-2">
              <label
                className="hidden text-sm block text-gray-600"
                htmlFor="cus_email"
              >
                City
              </label>
              <input
                className="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded"
                id="cus_email"
                name="cus_email"
                type="text"
                required=""
                placeholder="City"
                aria-label="Email"
                required
              />
            </div>
            <div className="inline-block mt-2 w-1/2 pr-1">
              <label
                className="hidden block text-sm text-gray-600"
                htmlFor="cus_email"
              >
                Country
              </label>
              <input
                className="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded"
                id="cus_email"
                name="cus_email"
                type="text"
                required=""
                placeholder="Country"
                aria-label="Email"
                required
              />
            </div>
            <div className="inline-block mt-2 -mx-1 pl-1 w-1/2">
              <label
                className="hidden block text-sm text-gray-600"
                htmlFor="cus_email"
              >
                Zip
              </label>
              <input
                className="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded"
                id="cus_email"
                name="cus_email"
                type="text"
                required=""
                placeholder="Zip"
                aria-label="Email"
                required
              />
            </div>
            <p className="mt-4 text-gray-800 font-medium">Payment Method</p>
            <div className="grid">
              <button className="p-1 pl-4 pr-4 bg-transparent border-2 border-blue-500 text-blue-500 text-lg rounded-lg hover:bg-blue-500 hover:text-gray-100 focus:border-4 focus:border-blue-300">
                Cash On Delivery
              </button>
              <button
                className="p-1 pl-6 pr-6 bg-transparent border-2 border-yellow-500 text-yellow-500 text-lg rounded-lg hover:bg-yellow-500 hover:text-gray-100 focus:border-4 focus:border-yellow-300"
                onClick={() => setBankVisible(!bankVisible)}
              >
                Online Transfer
              </button>
              <input
                className={`${
                  !bankVisible && 'hidden'
                } w-full px-2 py-2 text-gray-700 bg-gray-200 rounded`}
                id="cus_name"
                name="cus_name"
                type="number"
                required=""
                placeholder="Card Number MM/YY CVC"
                aria-label="Name"
              />
            </div>
            <div className="flex justify-between mt-4">
              <button
                className="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded"
                onClick={() => history.push('/products')}
              >
                Back
              </button>
              <button
                className="flex items-center justify-center px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded"
                onClick={checkout}
              >
                {loading && (
                  <Loader
                    type="TailSpin"
                    color="#00BFFF"
                    height={20}
                    width={20}
                  />
                )}
                <span className={`${loading && 'pl-3'}`}>Confirm</span>
              </button>
            </div>
          </div>
        </div>
        <div className="col-span-1 bg-white lg:block hidden">
          <h1 className="py-6 border-b-2 text-xl text-gray-600 px-8">
            Order Summary
          </h1>
          <ul className="py-6 border-b space-y-6 px-8">
            {cart.map((item) => {
              return (
                <CartItem
                  id={item.id}
                  photo={item.photo}
                  productName={item.productName}
                  description={item.description}
                  count={item.count}
                  price={item.cup.price}
                  size={item.cup.size}
                  cupId={item.cup._id}
                  reduceQuantity={reduceQuantity}
                  addQuantity={addQuantity}
                />
              );
            })}
          </ul>
          <div className="px-8 border-b">
            <div className="flex justify-between py-4 text-gray-600">
              <span>Delivery fee</span>
              <span className="font-semibold text-pink-500">Free</span>
            </div>
          </div>
          <div className="font-semibold text-xl px-8 flex justify-between py-8 text-gray-600">
            <span>Total</span>
            <span>₱ {total.toFixed(2)}</span>
          </div>
        </div>
      </section>
    </div>
  );
};
export default Cart;
