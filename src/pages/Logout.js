import React from 'react';
import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

const Logout = () => {
  const { unsetUser, setUser } = useContext(UserContext);

  unsetUser();

  useEffect(() => {
    setUser(null);
    localStorage.removeItem('user');
  });

  return <Redirect to="/login" />;
};

export default Logout;
