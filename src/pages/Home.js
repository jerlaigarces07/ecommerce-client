import React from 'react';
import affogato from '../images/affogato.png';
import caffemocha from '../images/caffemocha.png';
import Pulse from 'react-reveal/Pulse';
import coffeegirl from '../images/coffeegirl.gif';
import americano1 from '../images/americano1.png';
import shop1 from '../images/shop1.jpg';
import shop2 from '../images/shop2.jpg';
import shop3 from '../images/shop3.jpg';
import shop4 from '../images/shop4.jpg';
import shop5 from '../images/shop5.jpg';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';

export default function Home() {
  const [contactEmail, setContactEmail] = useState('');
  const [contactMessage, setContactMessage] = useState('');
  const [isActive, setIsActive] = useState(false);

  const handleSubmit = () => {
    if (contactEmail !== '' && contactMessage !== '') {
      Swal.fire({
        icon: 'success',
        title: 'Message has been sent',
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Cannot send message',
      });
    }
    setContactEmail('');
    setContactMessage('');
  };

  useEffect(() => {
    if (contactEmail !== '' && contactMessage !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [contactEmail, contactMessage]);

  return (
    <>
      <section className="relative bg-blueGray-50 w-full banner">
        <div
          className="relative flex content-center items-center justify-center bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 w-full"
          id="clipBackground"
        >
          <div className="container relative mx-auto">
            <div className="items-center flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                <div className="pb-40">
                  <h1 className="text-pink-700 text-1xl font-semibold md:text-5xl">
                    Sit, Relax, and Sip a cup of coffee!
                  </h1>
                  <p className="mt-4 text-lg text-blueGray-200 font-light">
                    "Once you wake up and smell the coffee, it's hard to go back
                    to sleep." - Fran Drescher
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 py-14">
          <h3 class="text-2xl tracking-widest text-blue-600 text-center">
            TRY NOW!
          </h3>
          <h1 class="mt-8 text-center text-5xl text-blue-600 font-bold">
            Our Featured Products:
          </h1>

          <div class="md:flex md:justify-center md:space-x-8 md:px-14">
            <div class="mt-16 py-4 px-4 bg-whit w-72 bg-white rounded-xl shadow-lg hover:shadow-xl transform hover:scale-110 transition duration-500 mx-auto md:mx-0">
              <div class="w-sm">
                <img class="w-64" src={affogato} alt="" />
                <div class="mt-4 text-blue-600 text-center">
                  <h1 class="text-xl font-bold">AFFOGATO</h1>
                  <p class="mt-4 text-gray-600">
                    Espresso poured on a vanilla ice cream. Served in a
                    cappuccino cup.
                  </p>
                </div>
              </div>
            </div>

            <div class="mt-16 py-4 px-4 bg-whit w-72 bg-white rounded-xl shadow-lg hover:shadow-xl transform hover:scale-110 transition duration-500 mx-auto md:mx-0">
              <div class="w-sm">
                <img class="w-64" src={caffemocha} alt="" />
                <div class="mt-4 text-blue-600 text-center">
                  <h1 class="text-xl font-bold">CAFFÈ MOCHA</h1>
                  <p class="mt-4 text-gray-600">
                    A caffè latte with chocolate and whipped cream, made by
                    pouring about 2 cl of chocolate sauce into the glass,
                    followed by an espresso shot and steamed milk.
                  </p>
                </div>
              </div>
            </div>

            <div class="mt-16 py-4 px-4 bg-whit w-72 bg-white rounded-xl shadow-lg hover:shadow-xl transform hover:scale-110 transition duration-500 mx-auto md:mx-0">
              <div class="w-sm">
                <img class="w-64" src={americano1} alt="" />
                <div class="mt-4 text-blue-600 text-center">
                  <h1 class="text-xl font-bold">AMERICANO</h1>
                  <p class="mt-4 text-gray-600">
                    Espresso with added hot water (100–150 ml). Often served in
                    a cappuccino cup.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Products section */}
      <section
        className="relative bg-center text-blueGray-700 h-screen"
        id="gallery"
      >
        <div className="relative grid grid-cols-2 md:grid-cols-7 md:grid-flow-row gap-4 bg-white place-items-center p-5">
          <div className="col-start-1 col-end-2 md:row-span-3 md:col-span-3 w-full pt-0 mb-16 text-left">
            <img
              src={coffeegirl}
              alt="lady with coffee"
              className="lg:w-1/2 h-1/2"
            />
            <h1 className="text-center mb-2 md:mb-8 text-1xl md:text-2xl font-black tracking-tighter text-black lg:text-5xl title-font">
              {' '}
              Love is in the air, and it smells like coffee..{' '}
            </h1>
            <div className="grid place-content-center">
              <a href="/products">
                <button className="p-2 pl-5 pr-5 bg-transparent border-2 border-blue-500 text-blue-500 text-sm md:text-lg rounded-lg hover:bg-blue-500 hover:text-gray-100 focus:border-4 focus:border-blue-300">
                  Check all products
                </button>
              </a>
            </div>
          </div>
          <div className="md:col-span-4 h-auto w-full md:h-full">
            <div className="bg-white shadow-md p-3 h-96">
              <img src={shop1} alt="" className="w-full h-full object-cover" />
            </div>
          </div>
          <div className="md:col-span-2 w-full h-full">
            <div className="bg-white shadow-md p-3">
              <img src={shop2} alt="" className="w-full h-full object-cover" />
            </div>
          </div>
          <div className="md:col-span-2">
            <div className="bg-white shadow-md p-3">
              <img src={shop3} alt="" />
            </div>
          </div>
          <div className="md:col-span-2">
            <div className="bg-white shadow-md p-3">
              <img src={shop4} alt="" />
            </div>
          </div>
          <div className="md:col-span-2 w-full h-full">
            <div className="bg-white shadow-md p-3">
              <img src={shop5} alt="" className="w-full h-full object-cover" />
            </div>
          </div>
        </div>
      </section>
      {/* Contact */}
      <div
        class="h-screen bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 py-6 flex flex-col justify-center md:-mt-96 sm:py-12 lg:mt-20"
        id="contact"
      >
        <div className="flex justify-center mt-20">
          <div class="relative py-3 sm:max-w-xl sm:mx-auto mt-10">
            <div class="absolute inset-0 bg-gradient-to-r from-blue-300 to-blue-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
            <div class="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
              <div class="max-w-md mx-auto">
                <div>
                  <h1 class="text-2xl font-semibold">
                    Send us a message and our Sales team will get back to you
                    asap!
                  </h1>
                </div>
                <div class="divide-y divide-gray-200">
                  <div class="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                    <div class="relative">
                      <input
                        autocomplete="off"
                        value={contactEmail}
                        name="email"
                        type="text"
                        class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:border-rose-600 font-light text-sm"
                        placeholder="Email address"
                        onChange={(e) => setContactEmail(e.target.value)}
                      />
                      <label
                        for="email"
                        class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                      >
                        Email Address
                      </label>
                    </div>
                    <div class="relative">
                      <label
                        for="contactMessage"
                        className="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
                      >
                        Message
                      </label>
                      <input
                        required
                        name="message"
                        value={contactMessage}
                        className="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-800 focus:outline-none focus:border-rose-600 font-light text-sm"
                        placeholder="Enter product description"
                        spellcheck="false"
                        onChange={(e) => setContactMessage(e.target.value)}
                      />
                    </div>
                    <div className="flex justify-center items-end">
                      {isActive ? (
                        <button
                          class="p-2 pl-5 pr-5 bg-transparent border-2 border-blue-500 text-blue-500 text-lg rounded-lg hover:bg-blue-500 hover:text-gray-100 focus:border-4 focus:border-blue-300"
                          onClick={() => handleSubmit()}
                        >
                          Submit
                        </button>
                      ) : (
                        <button
                          class="p-2 pl-5 pr-5 bg-transparent border-2 border-blue-500 text-blue-500 text-lg rounded-lg hover:bg-blue-500 hover:text-gray-100 focus:border-4 focus:border-blue-300"
                          disabled
                          onClick={() => handleSubmit()}
                        >
                          Submit
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
