import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { useHistory } from 'react-router-dom';
import { Redirect } from 'react-router';
import Swal from 'sweetalert2';
import ProductContext from '../ProductContext';
import UserContext from '../UserContext';

const EditProduct = () => {
  const { editProduct } = useContext(ProductContext);
  const { user } = useContext(UserContext);
  const history = useHistory();

  const [productName, setProductName] = useState('');
  const [productId, setProductId] = useState('');
  const [selectedImage, setSelectedImage] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [cups, setCups] = useState([
    {
      _id: uuidv4(),
      size: '',
      price: '',
    },
  ]);

  const handleSubmit = async () => {
    const value = {
      productName: productName,
      description: productDescription,
      cup: getCupsList(),
      photo: selectedImage,
    };

    const response = await axios.put(
      `https://salty-bayou-48466.herokuapp.com/products/${productId}`,
      value,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );

    if (response.status === 200 && response.data.message === false) {
      Swal.fire({
        icon: 'error',
        title: 'Add Product Failed',
      });
    } else if (
      response.status === 200 &&
      response.data.message === 'Successfully updated'
    ) {
      Swal.fire({
        icon: 'success',
        title: 'Edit Product Successful',
      });
      setProductName('');
      setSelectedImage('');
      setProductDescription('');
      setCups([]);
    }
  };

  const [product, setProduct] = useState({
    id: null,
    isActive: null,
  });

  const handleCupSizeChange = (id, e) => {
    const newList = cups.map((cup) => {
      return cup._id === id ? { ...cup, size: e.target.value } : cup;
    });
    setCups(newList);
  };

  const handleCupPriceChange = (id, e) => {
    const newList = cups.map((cup) => {
      return cup._id === id ? { ...cup, price: e.target.value } : cup;
    });

    setCups(newList);
  };

  const handleRemoveCup = (id) => {
    const filter = cups.filter((cup) => cup._id != id);
    setCups(filter);
  };

  const handleAddCup = () => {
    if (cups.length === 4) return;

    setCups((prevCups) => [
      ...prevCups,
      {
        _id: uuidv4(),
        size: '',
        price: '',
      },
    ]);
  };

  const getCupsList = () => {
    return cups.map((data) => {
      return { size: data.size, price: data.price };
    });
  };

  const handleFileChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        setSelectedImage(e.target.result);
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onEditProduct = (e) => {
    e.preventDefault();

    fetch('https://salty-bayou-48466.herokuapp.com/products', {
      method: 'PUSH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productName: productName,
        decription: productDescription,
        cup: cups,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.message === 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveProductDetails(data.access);

          Swal.fire({
            icon: 'success',
            title: 'Successfully Updated',
            text: 'You will be directed to the product list..',
          }).then((result) => {
            if (result.isConfirmed) {
              history.push('/products');
            }
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'Make sure all required fields are filled.',
          });
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });

    setProductName('');
    setCups('');
  };

  const retrieveProductDetails = (token) => {
    fetch('https://salty-bayou-48466.herokuapp.com/products', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProduct({
          id: data._id,
          isActive: data.isActive,
        });
      });
  };

  useEffect(() => {
    setProductName(editProduct.productName);
    setSelectedImage(editProduct.photo);
    setProductDescription(editProduct.description);
    setCups(editProduct.cup);
    setProductId(editProduct.id);
  }, []);

  return product.id != null ? (
    <Redirect to="/products" />
  ) : (
    <>
      <div className="flex bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-banner justify-center">
        <div className="container m-10 max-w-md w-full space-y-8 p-10 bg-white rounded-xl shadow-lg z-10 h-auto">
          <div
            className="flex flex-col flex-initial"
            onSubmit={(e) => onEditProduct(e)}
          >
            <div className="flex flex-col">
              <div className="flex flex-col sm:flex-row items-center">
                <h2 className="font-semibold text-lg mr-auto">ADD A PRODUCT</h2>
                <div className="w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0"></div>
              </div>
              <div className="mt-5">
                <div className="form">
                  <div className="md:space-y-2 mb-3">
                    <div className="flex items-center py-6">
                      <div className="w-12 h-12 mr-4 flex-none rounded-xl border overflow-hidden">
                        <img
                          className="w-12 h-12 mr-4 object-cover"
                          src={selectedImage}
                          alt=""
                        />
                      </div>
                      <label htmlFor="img" className="cursor-pointer ">
                        <span className="focus:outline-none text-white text-sm py-2 px-4 rounded-full bg-green-400 hover:bg-green-500 hover:shadow-lg">
                          Browse
                        </span>
                        <input
                          id="img"
                          type="file"
                          name="img"
                          className="hidden"
                          accept="accept"
                          onChange={handleFileChange}
                          required
                        />
                      </label>
                    </div>
                  </div>
                  <div className="md:flex flex-row md:space-x-4 w-full text-xs">
                    <div className="mb-3 space-y-2 w-full text-xs">
                      <label className="font-semibold text-gray-600 py-2">
                        Product Name: <abbr title="required">*</abbr>
                      </label>
                      <input
                        placeholder="Product Name"
                        className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4 uppercase"
                        required="required"
                        type="text"
                        id="newProductName"
                        value={productName}
                        onChange={(e) => setProductName(e.target.value)}
                      />
                      <p className="text-red text-xs hidden">
                        Please fill out this field.
                      </p>
                    </div>
                  </div>
                  <div className="md:flex flex-row md:space-x-4 w-full text-xs">
                    <div className="mb-3 space-y-2 w-full text-xs">
                      <label className="font-semibold text-gray-600 py-2">
                        Cup Size: <abbr title="required">*</abbr>
                      </label>
                    </div>
                    <div className="mb-3 space-y-2 w-full text-xs">
                      <label className="font-semibold text-gray-600 py-2">
                        Price: <abbr title="required">*</abbr>
                      </label>
                    </div>
                  </div>
                  {cups.map((data) => (
                    <>
                      <div className="md:flex flex-row md:space-x-4 w-full text-xs">
                        <div className="mb-3 space-y-2 w-full text-xs">
                          <input
                            placeholder="Size"
                            className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4"
                            required="required"
                            type="text"
                            value={data.size}
                            onChange={(event) =>
                              handleCupSizeChange(data._id, event)
                            }
                          />
                          <p className="text-red text-xs hidden">
                            Please fill out this field.
                          </p>
                        </div>
                        <div className="mb-3 space-y-2 w-full text-xs">
                          <input
                            placeholder="Price"
                            className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg h-10 px-4"
                            required="required"
                            type="number"
                            name="cupPrice"
                            value={data.price}
                            onChange={(event) =>
                              handleCupPriceChange(data._id, event)
                            }
                          />
                          <p className="text-red text-xs hidden">
                            Please fill out this field.
                          </p>
                        </div>
                        <button onClick={() => handleRemoveCup(data._id)}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-5 w-5 mb-1 text-red-700 hover:text-red-400"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 000 2h6a1 1 0 100-2H7z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </button>
                      </div>
                    </>
                  ))}
                  <div className="text-right md:space-x-3 flex justify-center">
                    <button
                      className="mb-2 md:mb-0 bg-green-400 px-5 py-2 text-xs shadow-sm font-light tracking-wider text-white rounded-full hover:shadow-lg hover:bg-green-500"
                      onClick={handleAddCup}
                    >
                      Add size & price
                    </button>
                  </div>
                  <div className="flex-auto w-full mb-1 text-xs space-y-2">
                    <label className="font-semibold text-gray-600 py-2">
                      Description
                    </label>
                    <textarea
                      required=""
                      value={productDescription}
                      name="message"
                      id="productDescription"
                      className="w-full min-h-[100px] max-h-[300px] h-28 appearance-none block bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg  py-4 px-4"
                      placeholder="Enter product description"
                      spellcheck="false"
                      onChange={(e) => setProductDescription(e.target.value)}
                    ></textarea>
                  </div>
                  <p className="text-xs text-red-500 text-right my-3">
                    Required fields are marked with an asterisk{' '}
                    <abbr title="Required field">*</abbr>
                  </p>
                  <div className="mt-5 text-right md:space-x-3 md:block flex flex-col-reverse">
                    <a href="/admin">
                      <button className="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100">
                        Cancel
                      </button>
                    </a>
                    <button
                      className="mb-2 md:mb-0 bg-green-400 px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-green-500"
                      onClick={() => handleSubmit()}
                    >
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditProduct;
