import React, { useEffect, useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import { useHistory } from 'react-router-dom';

const History = () => {
  const history = useHistory();
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    (async () => {
      const response = await axios.get(
        'https://salty-bayou-48466.herokuapp.com/users/myOrders',
        {
          headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
        }
      );
      if (response.data) setOrders(response.data);
    })();
  }, []);

  console.log('order', orders);

  return typeof orders === 'string' ? (
    <div className="mt-10 max-w-3xl mx-auto">
      <p className="text-center mb-4 font-bold text-gray-600 text-2xl">
        You currently have no past orders
      </p>
      <button
        className="transform hover:scale-110 motion-reduce:transform-none hover:bg-blue-400 w-full bg-blue-500 text-white p-3 rounded-lg font-semibold text-lg"
        onClick={() => history.push('/products')}
      >
        Go to shop
      </button>
    </div>
  ) : (
    <>
      <p className="text-2xl text-center font-semibold text-gray-600 mt-20 mb-4">
        Your order history
      </p>
      {orders.map((order) => (
        <>
          <div className="bg-white h-full w-full">
            <div className="flex flex-col w-full max-w-6xl mx-auto p-4 shadow-md mb-4 rounded-lg bg-pink-100">
              <div className="flex w-full justify-between">
                <p className="text-lg text-gray-600 font-bold">
                  Purchased On :{' '}
                  {moment(order.purchasedOn).format('MMMM d, YYYY HH:mm:ss A')}
                </p>
                <p className="text-lg text-gray-600 font-bold">
                  Total Amount : ₱ {order.totalAmount.toFixed(2)}
                </p>
              </div>
              <div className="flex flex-wrap w-full p-3 justify-evenly">
                {order.products.map((product) => (
                  <div className="flex w-2/5 flex-col max-h-52 mt-4 transition duration-500 ease-in-out transform hover:-translate-y-2">
                    <div className="bg-white shadow-md  rounded-3xl p-4 h-full">
                      <div className="flex-none lg:flex h-full">
                        <div className="h-full w-full mb-3">
                          <img
                            src={product.photo}
                            alt=""
                            className=" w-full h-full min-w-20 object-cover lg:object-cover rounded-2xl"
                          />
                        </div>
                        <div className="flex flex-col ml-3 justify-evenly py-2">
                          <div className="flex flex-wrap ">
                            <h2 className="flex-auto text-lg font-medium">
                              {product.productName}
                            </h2>
                          </div>
                          <p className="mt-3">{product.description}</p>
                          <div className="flex pt-4 justify-between mt-auto text-sm text-gray-500">
                            <p className="text-gray-700 font-semibold">
                              {product.cup.size} ({product.count}{' '}
                              {product.count > 1 ? 'pcs' : 'pc'})
                            </p>
                            <p className="text-green-500 font-bold">
                              ₱ {(product.cup.price * product.count).toFixed(2)}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </>
      ))}
    </>
  );
};

export default History;
