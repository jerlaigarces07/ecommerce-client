import React from 'react';
import coffee from '../images/coffee.gif';
import { Redirect } from 'react-router';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';

const Register = () => {
  const history = useHistory();

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch('https://salty-bayou-48466.herokuapp.com/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.message === 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            icon: 'success',
            title: 'Successfully Registered!',
            text: 'Please log in using the email and password created.',
          }).then((result) => {
            if (result.isConfirmed) {
              history.push('/login');
            }
          });
        } else if (data.message === 'User already exists') {
          Swal.fire({
            icon: 'error',
            title: 'Duplicate email found',
            text: 'Please provide a different email.',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'Please check your information.',
          });
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });

    setFirstName('');
    setLastName('');
    setEmail('');
    setMobileNo('');
    setPassword1('');
    setPassword2('');
  }

  const retrieveUserDetails = (token) => {
    fetch('https://salty-bayou-48466.herokuapp.com/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (
      firstName !== 'null' &&
      lastName !== 'null' &&
      email !== 'null' &&
      mobileNo !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2]);

  return user.id != null ? (
    <Redirect to="/login" />
  ) : (
    <>
      <div className="bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-full">
        <section className="max-w-4xl p-6 mx-auto bg-cyan-700 rounded-md shadow-md dark:bg-gray-800 mt-40">
          <div className="flex flex-col items-center">
            <h1 className="text-xl font-bold text-white dark:text-white">
              Sign up today and get free coffee!
            </h1>
            <img src={coffee} alt="coffee.gif" className="w-20" />
          </div>

          <form onSubmit={(e) => registerUser(e)}>
            <div className="grid grid-cols-1 gap-6 mt-4 sm:grid-cols-2">
              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="firstName"
                >
                  First Name
                </label>
                <input
                  id="firstName"
                  type="text"
                  value={firstName}
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  onChange={(e) => setFirstName(e.target.value)}
                  required
                />
              </div>

              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="lastName"
                >
                  Last Name
                </label>
                <input
                  id="lastName"
                  type="text"
                  value={lastName}
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  onChange={(e) => setLastName(e.target.value)}
                  required
                />
              </div>

              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="email"
                >
                  Email Address
                </label>
                <input
                  id="emaill"
                  type="email"
                  value={email}
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </div>

              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="mobileNo"
                >
                  Mobile Number
                </label>
                <input
                  id="mobileNo"
                  type="tel"
                  value={mobileNo}
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  onChange={(e) => setMobileNo(e.target.value)}
                  required
                />
              </div>
              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="password1"
                >
                  Create Password
                </label>
                <input
                  id="password1"
                  type="password"
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  value={password1}
                  onChange={(e) => setPassword1(e.target.value)}
                  required
                />
              </div>
              <div>
                <label
                  className="text-white dark:text-gray-200"
                  htmlFor="password2"
                >
                  Confirm Password
                </label>
                <input
                  id="password2"
                  type="password"
                  className="border border-transparent focus:outline-none focus:ring-2 focus:ring-blue-900 block w-full px-4 py-2 mt-2 rounded-md"
                  value={password2}
                  onChange={(e) => setPassword2(e.target.value)}
                  required
                />
              </div>
            </div>

            {isActive ? (
              <div className="flex justify-center mt-6">
                <button className="transform hover:scale-110 motion-reduce:transform-none px-6 py-2 leading-5 text-white bg-pink-600 rounded-md hover:bg-pink-300">
                  Register
                </button>
              </div>
            ) : (
              <div className="flex justify-center mt-6">
                <button
                  className="transform hover:scale-110 motion-reduce:transform-none px-6 py-2 leading-5 text-white bg-pink-600 rounded-md disabled:opacity-50"
                  disabled
                >
                  Register
                </button>
              </div>
            )}
            <p className="pt-4 text-center text-sm font-extralight leading-relaxed">
              Already have an account?{' '}
              <a className="underline" href="/login">
                Sign in
              </a>
            </p>
          </form>
        </section>
      </div>
    </>
  );
};

export default Register;
