import React, { useContext } from 'react';
import { Fragment } from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';
import ProductCard from '../components/ProductCard';
import { difference, initial } from 'lodash';
import { ToastContainer, toast } from 'react-toastify';
import UserContext from '../UserContext';
import CartContext from '../CartContext';
import { useHistory, Redirect } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import Loader from 'react-loader-spinner';

const Products = () => {
  const { cartItems, setCartItems } = useContext(CartContext);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const addToCart = (data) => {
    const initialData = {
      ...data,
      count: 1,
    };

    const filter = cartItems.filter((item) => {
      return item.id === data.id && item.cup._id === data.cup._id;
    });

    if (filter.length === 0) {
      setCartItems((prevCartItems) => {
        localStorage.setItem(
          'cart',
          JSON.stringify([...prevCartItems, initialData])
        );
        return [...prevCartItems, initialData];
      });
    } else {
      const updatedQty = cartItems.map((item) => {
        if (item.id === data.id && item.cup._id === data.cup._id) {
          return {
            ...item,
            count: item.count + 1,
          };
        }
        return item;
      });
      setCartItems(updatedQty);
      localStorage.setItem('cart', JSON.stringify(updatedQty));
    }
    toast('Nice! Product has been added to cart. 😍😍😍');
  };

  useEffect(() => {
    setLoading(true);
    (async () => {
      try {
        const response = await axios.get(
          'https://salty-bayou-48466.herokuapp.com/products'
        );
        setProducts(response.data);
        setLoading(false);
      } catch (error) {
        console.log('Error occurred', error);
      }
      setLoading(false);
    })();
  }, []);

  return (
    <>
      {loading ? (
        <div className="flex flex-col w-full h-screen items-center justify-center bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200">
          <Loader type="Hearts" color="#d61818" height={80} width={80} />
          <h1 className="text-gray-800 font-light">Loading.. please wait..</h1>
        </div>
      ) : (
        <div className="bg-gradient-to-br from-pink-100 via-rose-100 to-cyan-200 absolute w-full h-full">
          <div className="flex flex-wrap gap-1 md:gap-3 place-items-center mt-20 ml-5 mr-5 justify-center">
            <Fragment>
              {products.map((data) => (
                <ProductCard
                  productName={data.productName}
                  description={data.description}
                  cup={data.cup}
                  photo={data.photo}
                  id={data._id}
                  addToCart={addToCart}
                />
              ))}
            </Fragment>
          </div>
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </div>
      )}
    </>
  );
};

export default Products;
