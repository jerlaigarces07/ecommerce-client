import React, { useState, useEffect } from 'react';
import { Fragment } from 'react';
import logo from '../images/logo.png';
import { useHistory } from 'react-router-dom';

const Navbar = (props) => {
  const { cartItemCount } = props;
  const history = useHistory();
  const [accountVisible, setAccountVisible] = useState(false);
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    const userObject = localStorage.getItem('user');
    setCurrentUser(JSON.parse(userObject));
  }, [accountVisible]);

  return (
    <>
      <div className="flex flex-wrap place-items-start w-screen fixed z-50 top-0">
        <section className="relative mx-auto w-full">
          <nav className="flex justify-between bg-gradient-to-br from-pink-100 to-rose-100 text-dark w-full">
            <div className="px-4 py-2 flex w-full items-center">
              <a href="/">
                <img className="h-10" src={logo} alt="logo" />
              </a>
              <ul className="hidden md:flex px-4 mx-auto font-semibold font-heading space-x-12">
                <li>
                  <a className="hover:text-gray-200" href="/">
                    Home
                  </a>
                </li>
                <li>
                  <a className="hover:text-gray-200" href="/products">
                    Products
                  </a>
                </li>
                <li>
                  <a className="hover:text-gray-200" href="/#gallery">
                    Gallery
                  </a>
                </li>
                <li>
                  <a className="hover:text-gray-200" href="/#contact">
                    Contact Us
                  </a>
                </li>
              </ul>
              <div className="hidden md:flex items-center space-x-5">
                <a
                  className="hover:text-red-300 fill-current text-red-600"
                  href="#"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                    viewBox="0 0 20 20"
                  >
                    <path
                      fillRule="evenodd"
                      d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                      clipRule="evenodd"
                    />
                  </svg>
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                  />
                </a>
                <a
                  className="flex items-center hover:text-pink-300 fill-current text-pink-600"
                  href="/checkout"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6"
                    viewBox="0 0 20 20"
                  >
                    <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
                  </svg>
                  <span className="flex absolute -mt-5 ml-4">
                    <span className="animate-ping text-white absolute inline-flex h-4 w-4 rounded-full bg-red-400 opacity-75"></span>
                    <span className="relative inline-flex rounded-full h-4 w-4 bg-red-500"></span>
                    <div className="absolute -top-0.5 -right-1 inline-flex text-white text-sm rounded-full h-4 w-4">
                      {cartItemCount}
                    </div>
                  </span>
                </a>
                {/* Account */}
                <a
                  className="flex items-center hover:text-pink-300 fill-current text-pink-600"
                  href="#"
                  onClick={() => setAccountVisible(!accountVisible)}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-7 w-7 pointer-events-none"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                      clipRule="evenodd"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <a
              className="flex items-center hover:text-pink-300 fill-current text-pink-600 md:hidden"
              href="/checkout"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                viewBox="0 0 20 20"
              >
                <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
              </svg>
              <span className="flex absolute -mt-5 ml-4">
                <span className="animate-ping text-white absolute inline-flex h-4 w-4 rounded-full bg-red-400 opacity-75"></span>
                <span className="relative inline-flex rounded-full h-4 w-4 bg-red-500"></span>
                <div className="absolute -top-0.5 -right-1 inline-flex text-white text-sm rounded-full h-4 w-4">
                  {cartItemCount}
                </div>
              </span>
            </a>
            <a
              className="hover:text-pink-300 fill-current text-pink-600 navbar-burger self-center ml-5 mr-5 md:hidden"
              href="#"
              onClick={() => setAccountVisible(!accountVisible)}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
            </a>
            <div
              className={`${
                !accountVisible && 'hidden'
              } absolute right-0 mt-12 mr-4 py-2 w-48 bg-white rounded-md shadow-xl z-20`}
            >
              {currentUser !== null ? (
                <Fragment>
                  <button
                    className="block w-full px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                    onClick={() => {
                      history.push('/history');
                      setAccountVisible(false);
                    }}
                  >
                    History
                  </button>
                  <button
                    className="block w-full px-4 py-2 text-sm  capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                    onClick={() => {
                      history.push('/logout');
                      setAccountVisible(false);
                      setCurrentUser(null);
                    }}
                  >
                    Logout
                  </button>
                </Fragment>
              ) : (
                <Fragment>
                  <button
                    className="block w-full px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                    onClick={() => {
                      history.push('/register');
                      setAccountVisible(false);
                    }}
                  >
                    Register
                  </button>
                  <button
                    className="block w-full px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                    onClick={() => {
                      history.push('/login');
                      setAccountVisible(false);
                    }}
                  >
                    Login
                  </button>
                </Fragment>
              )}
            </div>
          </nav>
        </section>
      </div>
    </>
  );
};

export default Navbar;
