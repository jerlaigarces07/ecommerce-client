import React from 'react';

const CartItem = (props) => {
  const {
    photo,
    productName,
    count,
    price,
    size,
    id,
    cupId,
    reduceQuantity,
    addQuantity,
  } = props;

  const total = price * count;

  return (
    <li className="flex gap-2 border-b-1 max-h-40">
      <div className="self-center h-full">
        <img
          src={photo}
          alt="Product"
          className="rounded w-24 max-h-40 object-cover"
        />
      </div>
      <div className="flex flex-col col-span-3 pt-2 items-center justify-center">
        <span className="text-gray-600 text-md font-semi-bold">
          {productName} ({size})
        </span>
      </div>
      <div className="col-span-2 pt-3 ml-auto">
        <div className="flex h-full space-x-2 text-sm items-center justify-center">
          <span className="text-gray-400">{`${count} x ₱${price.toFixed(
            2
          )}`}</span>
          <span className="text-pink-400 font-semibold inline-block">
            ₱ {total.toFixed(2)}
          </span>
        </div>
      </div>
      <div className="flex items-center justify-center ml-auto">
        <button
          className="transition ease-in h-10 duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-red-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-red-600 ml-auto "
          onClick={() => reduceQuantity(id, count, cupId)}
        >
          -
        </button>

        <button
          className="transition ease-in h-10 duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-green-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-green-600 ml-2 "
          onClick={() => addQuantity(id, cupId)}
        >
          +
        </button>
      </div>
    </li>
  );
};

export default CartItem;
