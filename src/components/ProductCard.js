import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

const ProductCard = (props) => {
  const history = useHistory();
  const [selectedCup, setSelectedCup] = useState({});
  const {
    id,
    productName,
    description,
    cup,
    photo,
    isAdmin,
    setEditProduct,
    addToCart,
    fetchProducts,
  } = props;

  useEffect(() => {
    (() => {
      if (cup.length > 0) setSelectedCup(cup[0]);
    })();
  }, []);
  const handleDelete = async () => {
    try {
      fetch(`https://salty-bayou-48466.herokuapp.com/products/${id}/archive`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log('data', data);
          if (data.message === false) {
            Swal.fire({
              icon: 'error',
              title: 'Delete Product Failed',
            });
          } else if (data.message === 'Product has been archived') {
            fetchProducts();
            Swal.fire({
              icon: 'success',
              title: 'Delete Product Successful',
            });
          }
        });
    } catch (error) {}
  };

  return (
    <>
      <div className="container max-w-xs">
        <div className="flex flex-col bg-pink-100 shadow-lg rounded-xl w-9/12 p-6 md:w-96 lg:w-80 h-auto md:h-5/6">
          <div className="relative flex w-full mb-3 justify-center content-center">
            <img
              src={photo}
              alt=""
              className="w-3/12 md:w-9/12 xl:w-80 xl:h-80 rounded-2xl"
            />
          </div>
          <div className="flex-auto justify-evenly">
            <div className="flex flex-wrap ">
              <div className="flex items-center w-full justify-between min-w-0 text-xl md:text-2xl mb-2 font-semibold uppercase">
                <h1>{productName}</h1>
              </div>
              <div>
                <h2 className="text-sm md:text-lg mr-auto text-black font-light line-clamp-2">
                  {description}
                </h2>
              </div>
            </div>
            <div className="text-lg md:text-xl text-black font-semibold mt-1">
              Php {selectedCup.price}
            </div>
            <div className="lg:flex  py-4  text-sm text-gray-600">
              <div className="flex-1 inline-flex items-center mb-3">
                <span className="text-secondary whitespace-nowrap mr-3">
                  Size:
                </span>
                <div className=" flex cursor-pointer text-gray-400 ">
                  {cup.map((data) => (
                    <div
                      key={data._id}
                      className="hover:text-purple-500 px-2 py-1"
                      onClick={() => setSelectedCup(data)}
                    >
                      {data.size}
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="flex space-x-2 text-sm font-medium justify-start">
              {isAdmin ? (
                <>
                  <button
                    className="transition ease-in duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-purple-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-purple-600 "
                    onClick={() => {
                      setEditProduct({
                        id: id,
                        productName: productName,
                        description: description,
                        cup: cup,
                        photo: photo,
                      });
                      history.push('/admin/edit');
                    }}
                  >
                    <span>Edit</span>
                  </button>
                  <button
                    class="p-2 pl-5 pr-5 bg-transparent border-2 border-red-500 text-red-500 text-lg rounded-lg hover:bg-red-500 hover:text-gray-100 focus:border-4 focus:border-red-300"
                    onClick={handleDelete}
                  >
                    Delete
                  </button>
                </>
              ) : (
                <button
                  className="transition ease-in duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-purple-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-purple-600 "
                  onClick={() => {
                    addToCart({
                      id: id,
                      productName: productName,
                      description: description,
                      cup: selectedCup,
                      photo: photo,
                    });
                  }}
                >
                  <span>Add to Cart</span>
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductCard;

ProductCard.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
