import './App.css';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products';
import Admin from './pages/Admin';
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { ProductProvider } from './ProductContext';
import { CartProvider } from './CartContext';
import AddAProduct from './pages/AddAProduct';
import EditProduct from './pages/EditProduct';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import History from './pages/History';
import Error from './pages/Error';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  const [cartItems, setCartItems] = useState([]);

  const [editProduct, setEditProduct] = useState({
    productName: '',
    description: '',
    cup: [],
    photo: '',
  });

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <ProductProvider value={{ editProduct, setEditProduct }}>
        <CartProvider value={{ cartItems, setCartItems }}>
          <Router>
            <Navbar />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route exact path="/products" component={Products} />
              <Route exact path="/admin" component={Admin} />
              <Route exact path="/admin/add" component={AddAProduct} />
              <Route exact path="/admin/edit" component={EditProduct} />
              <Route exact path="/checkout" component={Cart} />
              <Route exact path="/history" component={History} />
              <Route component={Error} />
            </Switch>
          </Router>
        </CartProvider>
      </ProductProvider>
    </UserProvider>
  );
}

export default App;
